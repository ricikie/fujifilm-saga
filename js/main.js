        $("#toggle").click(function(){
            $(".toggle").toggleClass('toggle-active');
            $(".header2").toggleClass('header-active');
            $(".header__nav2").toggleClass('nav-active');
        }), function(){
            $(".toggle").removeClass('toggle-active');
            $(".header2").removeClass('header-active');
            $(".header__nav2").removeClass('nav-active');
        };

        var slideCount = $('#bottom-slider ul li').length;
        var slideWidth = $('#bottom-slider ul li').width();
        var slideHeight = $('#bottom-slider ul').height();
        var sliderUlWidth = slideCount * slideWidth;

        $('#bottom-slider ul li:first-child').addClass('active');


        function moveLeft() {
            $('#bottom-slider ul').animate({
                left: + slideWidth
            }, 100, function () {
                $('#bottom-slider ul li:last-child').prependTo('#bottom-slider ul');
                $('#bottom-slider ul').css('left', '');
                $('#bottom-slider ul li').removeClass('active');
                $('#bottom-slider ul li:first-child').addClass('active');
            });
        };

        function moveRight() {
            $('#bottom-slider ul').animate({
                left: - slideWidth
            }, 100, function () {
                $('#bottom-slider ul li:first-child').appendTo('#bottom-slider ul');
                $('#bottom-slider ul').css('left', '');
                $('#bottom-slider ul li').removeClass('active');
                $('#bottom-slider ul li:first-child').addClass('active');
            });
        };
        
        $('button.bottom-prev').click(function () {
            moveLeft();
        });
        
        $('button.bottom-next').click(function () {
            moveRight();
        });